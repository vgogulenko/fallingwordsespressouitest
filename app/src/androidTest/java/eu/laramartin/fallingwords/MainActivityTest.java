package eu.laramartin.fallingwords;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static eu.laramartin.fallingwords.R.id.text_counter_incorrect;


@RunWith(AndroidJUnit4.class)
public class MainActivityTest {


    @Rule
    public ActivityTestRule<MainActivity> mainActivityRule = new ActivityTestRule<>(MainActivity.class, false, false);

    /**
     * Unfortunately this test always runs because there is an animation on screen.
     * I wasn't able to run it even though I disabled all animations in my emulators.
     */


    @Test
    public void display_initial_content() {
        mainActivityRule.launchActivity(null);
        onView(withId(R.id.text_first_word)).check(matches(isDisplayed()));
        onView(withId(R.id.text_second_word)).check(matches(isDisplayed()));
        onView(withId(R.id.image_correct)).check(matches(isDisplayed()));
        onView(withId(R.id.image_incorrect)).check(matches(isDisplayed()));
        onView(withId(R.id.text_counter_correct)).check(matches(withText("0")));
        onView(withId(text_counter_incorrect)).check(matches(withText("0")));
        onView(withId(R.id.text_counter_unknown)).check(matches(withText("0")));
        onView(withId(R.id.image_correct)).perform(click());



    }
}
