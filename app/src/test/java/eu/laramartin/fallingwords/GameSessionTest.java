package eu.laramartin.fallingwords;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import eu.laramartin.fallingwords.data.WordPair;

import static junit.framework.Assert.assertEquals;

public class GameSessionTest {

    private GameSession gameSession;
    
    @Test
    public void check_WordPair_selected_in_GameSession_with_one_item() throws Exception {
        // given
        WordPair wordPair = WordPair.builder()
                .wordInEnglish("Hello")
                .wordInSpanish("Hola")
                .build();
        List<WordPair> wordPairs = new ArrayList<>();
        wordPairs.add(wordPair);
        gameSession = new GameSession(wordPairs);

        // then
        assertEquals(wordPair.wordInEnglish(), gameSession.actualEnglishWord());
        assertEquals(wordPair.wordInSpanish(), gameSession.actualSpanishWord());
    }

    @Test
    public void check_score_correct_on_correct_answer() throws Exception {
        // given
        WordPair wordPair = WordPair.builder()
                .wordInEnglish("Hello")
                .wordInSpanish("Hola")
                .build();
        List<WordPair> wordPairs = new ArrayList<>();
        wordPairs.add(wordPair);
        gameSession = new GameSession(wordPairs);

        // when
        gameSession.answer(true);

        // then
        assertEquals(1, gameSession.totalCorrectPairs());
        assertEquals(0, gameSession.totalIncorrectPairs());
        assertEquals(0, gameSession.totalUnknownPairs());
    }

    @Test
    public void check_score_correct_on_incorrect_answer() throws Exception {
        // given
        WordPair wordPair = WordPair.builder()
                .wordInEnglish("Hello")
                .wordInSpanish("Hola")
                .build();
        List<WordPair> wordPairs = new ArrayList<>();
        wordPairs.add(wordPair);
        gameSession = new GameSession(wordPairs);

        // when
        gameSession.answer(false);

        // then
        assertEquals(0, gameSession.totalCorrectPairs());
        assertEquals(1, gameSession.totalIncorrectPairs());
        assertEquals(0, gameSession.totalUnknownPairs());
    }

    @Test
    public void check_score_correct_on_unknown_answer() throws Exception {
        // given
        WordPair wordPair = WordPair.builder()
                .wordInEnglish("Hello")
                .wordInSpanish("Hola")
                .build();
        List<WordPair> wordPairs = new ArrayList<>();
        wordPairs.add(wordPair);
        gameSession = new GameSession(wordPairs);

        // when
        gameSession.noAnswerInTime();

        // then
        assertEquals(0, gameSession.totalCorrectPairs());
        assertEquals(0, gameSession.totalIncorrectPairs());
        assertEquals(1, gameSession.totalUnknownPairs());
    }
}
