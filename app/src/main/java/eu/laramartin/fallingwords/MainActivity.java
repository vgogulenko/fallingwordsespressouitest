package eu.laramartin.fallingwords;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.laramartin.fallingwords.utils.AnimationUtils;
import eu.laramartin.fallingwords.utils.VibrationUtils;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.image_correct)
    ImageView correct;
    @BindView(R.id.image_incorrect)
    ImageView incorrect;
    @BindView(R.id.text_first_word)
    TextView firstWord;
    @BindView(R.id.text_second_word)
    TextView secondWord;
    @BindView(R.id.text_counter_correct)
    TextView counterCorrect;
    @BindView(R.id.text_counter_incorrect)
    TextView counterIncorrect;
    @BindView(R.id.text_counter_unknown)
    TextView counterUnknown;

    private GameSession gameSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
        }
        ButterKnife.bind(this);

        startGameSession();
    }

    private void startGameSession() {
        gameSession = new GameSession(getAssets());
        updateUi();
        animateSecondWord();
    }

    private void animateSecondWord() {
        Animation slide = AnimationUtils.slide(secondWord);
        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                secondWord.clearAnimation();
                gameSession.noAnswerInTime();
                VibrationUtils.runLongVibration(MainActivity.this);
                next();
            }
        });
    }

    @OnClick(R.id.image_correct)
    public void onClickImageCorrect() {
        gameSession.answer(true);
        VibrationUtils.runShortVibration(this);
        next();
    }

    @OnClick(R.id.image_incorrect)
    public void onClickImageIncorrect() {
        gameSession.answer(false);
        VibrationUtils.runShortVibration(this);
        next();
    }

    private void updateUi() {
        firstWord.setText(gameSession.actualEnglishWord());
        secondWord.setText(gameSession.actualSpanishWord());

        counterCorrect.setText(String.valueOf(gameSession.totalCorrectPairs()));
        counterIncorrect.setText(String.valueOf(gameSession.totalIncorrectPairs()));
        counterUnknown.setText(String.valueOf(gameSession.totalUnknownPairs()));
    }

    private void next() {
        gameSession.next();
        updateUi();
        animateSecondWord();
    }
}
