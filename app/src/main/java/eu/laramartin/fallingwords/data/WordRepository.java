package eu.laramartin.fallingwords.data;

import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class WordRepository {

    private List<WordPair> words;

    public WordRepository(AssetManager assetManager) {
        try {
            Reader reader = new InputStreamReader(assetManager.open("words.json"));
            Gson gson = new GsonBuilder()
                    .registerTypeAdapterFactory(WordTypeAdapterFactory.create())
                    .create();
            Type listType = new TypeToken<ArrayList<WordPair>>() {}.getType();
            words = gson.fromJson(reader, listType);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<WordPair> getWords() {
        return words;
    }
}
