package eu.laramartin.fallingwords;

import android.content.res.AssetManager;
import android.support.annotation.VisibleForTesting;

import java.util.List;
import java.util.Random;

import eu.laramartin.fallingwords.data.WordPair;
import eu.laramartin.fallingwords.data.WordRepository;

public class GameSession {

    private List<WordPair> wordPairs;
    private WordPair actualWordPair;
    private String testWord;
    private Random random = new Random();
    private int totalCorrectAnswers;
    private int totalIncorrectAnswers;
    private int totalUnknownAnswers;

    @VisibleForTesting
    public GameSession(List<WordPair> wordPairs) {
        this.wordPairs = wordPairs;
        next();
    }

    public GameSession(AssetManager assets) {
        WordRepository repository = new WordRepository(assets);
        wordPairs = repository.getWords();
        next();
    }

    public void next() {
        int index = getRandomNumber(wordPairs.size());
        actualWordPair = wordPairs.get(index);
        testWord = getTestWord();
    }

    private int getRandomNumber(int max) {
        return random.nextInt(max);
    }

    private boolean getRandomBoolean() {
        return random.nextBoolean();
    }

    public String actualEnglishWord() {
        return actualWordPair.wordInEnglish();
    }

    public String actualSpanishWord() {
        return testWord;
    }

    private String getTestWord() {
        boolean randomBoolean = getRandomBoolean();
        if (randomBoolean) {
            // Return correct spanish word solution
            return actualWordPair.wordInSpanish();
        } else {
            // Return random spanish word
            // There is a small chance that the random word is the same as the correct word
            return wordPairs.get(getRandomNumber(wordPairs.size())).wordInSpanish();
        }
    }

    public boolean isTranslationCorrect() {
        return (testWord.equals(actualWordPair.wordInSpanish()));
    }

    // User answer: parameter is true when user taps "correct button"
    public void answer(boolean correct) {
        boolean result = isTranslationCorrect() == correct;
        if (result) {
            totalCorrectAnswers++;
        } else {
            totalIncorrectAnswers++;
        }
    }

    public void noAnswerInTime() {
        totalUnknownAnswers++;
    }

    public int totalCorrectPairs() {
        return totalCorrectAnswers;
    }

    public int totalIncorrectPairs() {
        return totalIncorrectAnswers;
    }

    public int totalUnknownPairs() {
        return totalUnknownAnswers;
    }

    @Override
    public String toString() {
        return "GameSession{" +
                "actualWordPair=" + actualWordPair +
                ", testWord='" + testWord + '\'' +
                ", totalCorrectAnswers=" + totalCorrectAnswers +
                ", totalIncorrectAnswers=" + totalIncorrectAnswers +
                ", totalUnknownAnswers=" + totalUnknownAnswers +
                '}';
    }
}
