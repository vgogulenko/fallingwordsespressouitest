package eu.laramartin.fallingwords.utils;

import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

public class AnimationUtils {

    public static Animation slide(final TextView textView) {
        Animation slide = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 1);

        slide.setDuration(5000);
        textView.startAnimation(slide);
        return slide;
    }
}
