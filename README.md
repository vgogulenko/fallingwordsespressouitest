# Android Falling Words 

<img src="https://raw.githubusercontent.com/laramartin/android_words/master/app/src/main/res/mipmap-xxhdpi/ic_launcher.png?token=ANQqdD2HEzA_E9iJp2SzIs4BXrtm9FpGks5Z20zYwA%3D%3D" width="70"/> 

## Description 

Falling Words is an Android App Game for learning Spanish. 

On App start an English word appears on top and a Spanish word below it. 
The Spanish word will fall to the bottom of the screen in 5 seconds. 
Within this time, the user can answer if the Spanish word is the correct translation. 

At the bottom of the screen there is a counter that displays the correct, incorrect and missed 
answers. 

<img src="https://raw.githubusercontent.com/laramartin/android_words/master/art/device-2017-10-01-182534.png" width="250"/><img src="https://raw.githubusercontent.com/laramartin/android_words/master/art/sample.gif" width="250"/>

## Structure

The project is divided in: 

* `WordRepository`: provides words to `GameSession`
* `GameSession`: implements the logic of the game
* `MainActivity`: displays UI

### WordRepository

Reads the JSON file, which contains a list of word pairs, one in English one in Spanish. 
The data is read from the file and parsed into a `List<WordPair>`. 
For this I used Gson and AutoValue with Gson extension dependencies, because these libraries 
autogenerate code facilitating the parsing. 

### GameSession

`GameSession` obtains a list of words from `WordRepository` or directly.
The game starts by selecting one random word pair and deciding if the second word shown is
going to be the correct answer or not. In the negative case, it selects a random word.

Then there are three possible paths: 
 1) the user taps the right button to answer
 2) the user taps the wrong button to answer 
 3) the user doesn't answer on time
 
`GameSession` stores a counter with the scores and implements the `next()` method to start the
next round.

There is a Unit Test implemented that verifies that `GameSession` with a single word pair works 
as expected. 

### MainActivity
Normal `MainActivity` that inflates and populates the layout using ButterKnife to bind the views. 
The layout uses `ConstraintLayout`. 
When `MainActivity` is created, the game starts. It also manages the animation. 

There is an Espresso UI Test to verify that the content is loaded on game start, but unfortunately 
I couldn't manage to make it work because the animation is always running on the screen, so the 
test does not continue. I haven't fixed it due to time constraints.  

## Considerations

**How much time was invested (within the given limit)**

7h 30min, measured with WakaTime Android Studio plugin.
 
**How was the time distributed (concept, model layer, view(s), game mechanics)**
 
 * Concept: 30 min taking notes on paper and Trello, thinking about the implementation
 * Model Layer: 2h thinking and implementing `WordRepository`, including learning how to load the JSON file
  on Android and the integration with AutoValue and Gson.
 * Game mechanics: 2h thinking and implementing the `GameSession` including the Unit Tests.
 * UI: 2h creating layouts and hooking together then `GameSession` and the UI, including creating the Espresso test
 and try to fix it.
 * Finishing touches: 1h for code cleanup, review and documentation.
 
**Decisions made to solve certain aspects of the game**

 * Created a separated `GameSession` for separation of concerns and testability.
 * Loaded the words on a list because it is easier to work with the data after.
 * Used Java Random to pick random words and decide if the Spanish word will be
 the right one or not on a 50% chance. The new random word is also picked using it.
 * Separated the JSON loading to a different class `WordRepository` to separate concerns.
 This allows to have different data sources.

**Decisions made because of restricted time**

Improvements I would do given more time:

 * Threading: load JSON file asynchronously  
 * Unit Tests: handle more test cases and with more words
 * Espresso tests: disable animations and implement more tests
 * Follow an Architecture pattern
 * Start/end game functionality: handle loading/error/empty view states, what to do before the
 game starts, what to do when the game ends
 * Handle configuration changes: currently the screen rotation is disabled and the activity does 
  not save the state, so the game would be restarted
 * Tablet: include different dimensions to support bigger screen sizes

**What would be the first thing to improve or add if there had been more time**

 * Fixing the Espresso test so it can run with animations on the screen.


## Thanks
Icons are from flaticon.com: 

* Launcher icon by <a href="http://www.freepik.com" target="_blank" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
* Cross icon by <a href="http://www.freepik.com" target="_blank" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
* Check mark icon by <a href="https://www.flaticon.com/authors/roundicons-freebies" target="_blank" title="Roundicons Freebies">Roundicons Freebies</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
